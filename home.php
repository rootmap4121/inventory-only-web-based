<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
	$fulldate=date('d/m/Y');
		$status=$_SESSION['SESS_STATUS'];
		
		
		//session_cache_limiter(1000);
			include("class_file/connection/config.php");
				
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php
include('title.php');
?>

<link rel="stylesheet" media="screen" href="css/reset.css" />
<link rel="stylesheet" media="screen" href="css/grid.css" />
<link rel="stylesheet" media="screen" href="css/style.css" />
<link rel="stylesheet" media="screen" href="css/messages.css" />
<link rel="stylesheet" media="screen" href="css/forms.css" />
<link rel="stylesheet" media="screen" href="css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>
<script type="text/javascript" src="js/jquery.tables.js"></script>
<script type="text/javascript" src="js/jquery.flot.js"></script>

<script type="text/javascript" src="js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
              <?php
			  
			  if($status==2){
			  

			    ?>
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                    <div class="main-content">
                      <section class="with-table">
                        <div class="container_6 clearfix">
                            <div class="grid_4 clearfix">
                                    <header class="clearfix">
                                   <br>
                                    <h3>Today's Statistics</h3>
                                    </header>
                                    <section>
                                    <div class="grid_1 alpha">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Stock In</h2></header>
                                            <section><h1><?php 
						echo mysql_num_rows(mysql_query("SELECT * FROM stockin_product WHERE spdate='$fulldate'"));  
											?></h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Stock Out</h2></header>
                                            <section><h1>
                                            <? 
											echo mysql_num_rows(mysql_query("SELECT * FROM stock_out WHERE stockoutdate='$fulldate'"));
											?>
                                            </h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Staff Requisation</h2></header>
                                            <section><h1>
                                            <?php
											echo mysql_num_rows(mysql_query("SELECT * FROM `staff_requisation` WHERE srdate='$fulldate'"));
											?>
                                            </h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1 omega">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Order</h2></header>
                                            <section><h1>
                                            <?php 
							echo mysql_num_rows(mysql_query("SELECT * FROM `order` WHERE o_date='$fulldate'"));
											?>
                                            </h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1 alpha">
                                           <div class="widget black ac">
                                            <header>
                                              <h2>Total Payment</h2></header>
                                            <section><h1><?php
                                     $sqltp=mysql_query("SELECT * FROM order_delivery_payment_record WHERE odprdate='$fulldate'");
									 $tp=0;
									 while($fettp=mysql_fetch_array($sqltp))
									 {
										 $tp+=$fettp['amount'];
									 }
									 echo $tp;
											?></h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Due Payment</h2></header>
                                            <section><h1>
                                            <?php
                                           $sqldp=mysql_query("SELECT * FROM `order` WHERE o_date='$fulldate'");
										   $dp=0;
										   while($dpfet=mysql_fetch_array($sqldp))
										   {
										   $dp+=$dpfet['due'];
										   }
										   echo $dp;
											?>
                                            </h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Re-Order Level</h2></header>
                                            <section><h1>
                                            <a href="report/reorderdatewiseandall.php"><?php


@$sql_bar=mysql_query("SELECT * FROM `product_barcode` order by pbid asc");
$a=0;
while($rr=mysql_fetch_array($sql_bar))
{
	$pbid=$rr['pbid'];
	$chk=mysql_num_rows(mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid'"));
	if($chk!=0)
	{
	@$sqlquery=mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid' order by spid DESC LIMIT 1");
	$pdata=mysql_fetch_array($sqlquery);
	
		$sql_qs=mysql_query("SELECT * FROM stockin_product WHERE barcode_id='$pbid'");
		$qs=0;
		while($qqs=mysql_fetch_array($sql_qs))
		{
			$qs+=$qqs['quantity'];	
		}
		//echo $qs;
		
		$sql_q=mysql_query("SELECT * FROM stock_out WHERE barcode_id='$pbid'");
		$q=0;
		while($qq=mysql_fetch_array($sql_q))
		{
			$q+=$qq['dquantity'];	
		}
		//$q;
		$reorder=$rr['reorder'];
		  
		  $resq=$qs-$q;
		  
		  if($resq>$reorder)
		  {
			$a+=0; 
		  }
		  else
		  {
			$a+=1;   
		  }									
		}
		  
}
echo $a;
		  
?></a>
                                            </h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1 omega">
                                     
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Daily Balance</h2></header>
                                            <section><h1> 
											<?php
                                           echo $ddsasd=$tp-$dp;
											?>
                                            </h1></section>
                                        </div>
                                    </div>
                                    </section>
                          </div>
                                <br>
                                
                          <div class="grid_2">
                            <h3>Site Total Statistics</h3>
                              <table class="simple full">
                                  <tr>
                                      <td width="40%" style="width: 30%">Total Staff</td>
                                      <td width="60%" style="width: 60%"><div class="progress progress-green"><span style="width: 100%"><b>
                                      <?php //echo mysql_num_rows(mysql_query("SELECT * FROM signup WHERE status='4'")); ?>
                                      </b></span></div></td>
                                  </tr> 
                                  <tr>
                                      <td style="width: 30%">Total Requsation</td>
                                      <td width="60%" style="width: 60%"><div class="progress progress-orange"><span style="width: 100%"><b><?php
                                           /*$fghhh=mysql_query("SELECT * FROM referal_income WHERE status='1'");
										   $xxxx=0;
										   while($fggg=mysql_fetch_array($fghhh))
										   {
										   $xxxx+=$fggg['income'];
										   }
										   echo $xxxx;*/
											?>
                                      </b></span></div></td>
                                  </tr>
                                  <tr>
                                      <td>Total Supplier</td><td><div class="progress progress-blue"><span style="width: 100%"><b>
                                      <?php
                                           /*$fghhhh=mysql_query("SELECT * FROM captcha_package_sold");
										   $xxxxh=0;
										   while($fgggh=mysql_fetch_array($fghhhh))
										   {
										   		$sdf_id=$fgggh['package_captcha_id'];
										   		$sdf=mysql_query("SELECT * FROM captcha_package WHERE id='$sdf_id'");
												while($sdf_f=mysql_fetch_array($sdf))
												{
												$xxxxh+=$sdf_f['price'];
												}
										   }
										   echo $xxxxh;*/
											?>
                                      </b></span></div></td>
                                  </tr>
                                  <tr>
                                      <td>Total Order</td><td><div class="progress progress-green"><span style="width: 100%"><b>
                                       <?php
                                           /*$orderpp=mysql_query("SELECT * FROM order_product");
										   $o_p=0;
										   while($opp=mysql_fetch_array($orderpp))
										   {
										   		$opid=$opp['cart_id'];
										   		$cartp=mysql_query("SELECT * FROM cart WHERE cart_id='$opid'");
												while($cartval=mysql_fetch_array($cartp))
												{
												$o_p+=$cartval['price'];
												}
										   }
										   echo $o_p;*/
											?>
                                      </b></span></div></td>
                                  </tr>
                                  <tr>
                                      <td>Total Stock IN </td><td><div class="progress progress-red"><span style="width: 100%"><b>
                                      <?php
                                       //echo mysql_num_rows(mysql_query("SELECT * FROM order_product WHERE status='0'"));
										   											?>
                                      </b></span></div></td>
                                  </tr><tr>
                                      <td>Total Stock Out </td>
                                      <td><div class="progress progress-blue"><span style="width: 100%"><b>
                                     <?php
                                      //echo mysql_num_rows(mysql_query("SELECT * FROM order_product WHERE status='1'"));
										   											?>
                                      </b></span></div></td>
                                  </tr>
                              </table>
                          </div>
                        </div>
                      </section>
                  </div>
              <div class="clear"></div>

                    <!-- Tables Section -->
                  <!-- End Tables Section -->
                </section>
                <?php
				}
				
				?>
                
              </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
    
    <?php include('footer.php'); ?>


    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
            <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
