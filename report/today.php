<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("../class_file/connection/config.php");	
	$access=$_SESSION['SESS_ID'];
	extract($_GET);
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('../title.php'); ?>

<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        <?php
			  $usr=$_SESSION['SESS_USERNAME'];

				@$sql_check_tab=mysql_num_rows(mysql_query("SELECT * FROM system_admin WHERE username='$usr'"));
				
				
				if($sql_check_tab!=0)
				{
			  ?>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
              <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>Today ( <?php echo $date; ?> ) </h2>
                        </header>
                        <section class="with-table">
                            <table width="97%" class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th width="55">ID</th>
                                      <th width="97" align="center">Barcode</th>
                                        <th width="104" align="center">Product</th>
                                        <th width="96">IN.Q</th>
                                        <th width="116">OUT.Q</th>
                                        <th width="127">Total Value</th>
                                      <th width="87">IN Date</th>
                                      <th width="99">Out.Date</th>
                                      <th>Status</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th height="28">ID</th>
                                      <th>Barcode</th>
                                        <th>Product</th>
                                        <th>IN.Q:
                                          <?php
                                        $sql_qsa=mysql_query("SELECT * FROM stockin_product WHERE spdate='$date'");
											$qsa=0;
											
											while($qqsa=mysql_fetch_array($sql_qsa))
											{
												$qsa+=$qqsa['quantity'];	
											}
											echo $qsa;
											
											?>
                                </th>
                                        <th>OUT.Q : 
                                        <?php
                                        $ss=mysql_query("SELECT * FROM stock_out WHERE stockoutdate='$date'");
											$sss=0;
											
											while($ssss=mysql_fetch_array($ss))
											{
												$sss+=$ssss['dquantity'];	
											}
											echo $sss;
											
											?>
                                        
                                        </th>
                                        <th width="127">Total  : 
                                        <?php
                                        $sql_qsas=mysql_query("SELECT * FROM stockin_product WHERE spdate='$date'");
											$qsas=0;
											
											while($qqsas=mysql_fetch_array($sql_qsas))
											{
												$qsas+=$qqsas['quantity']*$qqsas['unite_price'];	
											}
											echo $qsas;
											
											?>
                                        </th>
                                        <th>IN Date</th>
                                        <th>Out Date</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                                
                                <tbody>
                                <?php
								$a=1;
								@$sql_bar=mysql_query("SELECT * FROM `product_barcode` order by pbid asc");
								while($rr=mysql_fetch_array($sql_bar))
								{
									$pbid=$rr['pbid'];
									$chk=mysql_num_rows(mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid' AND spdate='$date'"));
									if($chk!=0)
									{
									@$sqlquery=mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid' AND spdate='$date' order by spid DESC LIMIT 1");
									$pdata=mysql_fetch_array($sqlquery);
									
									
									
									 ?>
										<tr>
											<td align="center"><?php echo $a;  ?></td>
											<td align="center"><?php 
											echo $rr['barcode'];
											?></td>
											<td align="center"><?php echo $rr['p_name']; ?></td>
											<td align="center"><?php
											$sql_qs=mysql_query("SELECT * FROM stockin_product WHERE barcode_id='$pbid' AND spdate='$date'");
											$qs=0;
											while($qqs=mysql_fetch_array($sql_qs))
											{
												$qs+=$qqs['quantity'];	
											}
											echo $qs;
											
											?></td>
											<td align="center"><?php
																			
											$sql_q=mysql_query("SELECT * FROM stock_out WHERE barcode_id='$pbid' AND stockoutdate='$date'");
											@$q=0;
											while($qq=mysql_fetch_array($sql_q))
											{
												$q+=$qq['dquantity'];	
											}
											echo @$q;
											?></td>
                                            <td align="center"><?php
											
											
											$sql_qd=mysql_query("SELECT * FROM stockin_product WHERE barcode_id='$pbid' AND spdate='$date'");
											$qd=0;
											while($qqd=mysql_fetch_array($sql_qd))
											{
												$qd+=$qqd['quantity']*$qqd['unite_price'];	
											}
											echo $qd;
											?></td>
										  <td align="center"><?php echo $pdata['spdate']; ?></td>
										  <td align="center"><?php 
										  
										  
									$dfg=mysql_query("SELECT * FROM stock_out WHERE barcode_id='$pbid' AND stockoutdate='$date' order by soid DESC LIMIT 1");
									$sdara=mysql_fetch_array($dfg);
										  echo $sdara['stockoutdate']; ?></td>
										  <td width="159" align="center">
                                          
                                           <a href="record_ind_datewise.php?id=<?php echo $pbid; ?>&amp;date=<?php echo $date; ?>&amp;p_name=<?php echo $rr['p_name']; ?>&amp;barcode=<?php echo $rr['barcode']; ?>" class="button button-gray">Record ( <?php echo $qs; ?> )</a>
                                          
                                          </td>
										</tr>
										
										<?php
									
											}
									$a++;
									}
									
									?>
                                </tbody>
                            </table>
                            <?php @$reorder=$rr['reorder'];
										  @$resq=$qs-$q;
										  
										  if($resq>$reorder)
										  {
											echo "<span style='background:green; color:white;'>".$reorder."</span>";  
										  }
										  else
										  {
											echo "<span style='background:red; color:white;'>".$reorder."</span>";   
										  }
										
										   ?>
                          <div class="container_6 clearfix">
                            
                          </div>
                      </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
            </div>

                <!-- Main Section End -->

            </div>
        </section>
        <?php
		}
		
		?>
    </div>
    
		<?php include('../footer.php'); ?>
    <!-- simple dialog -->

    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
