<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
include("class_file/connection/config.php");		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>The Qube Admin Panel</title> 

<link rel="stylesheet" media="screen" href="css/reset.css" />
<link rel="stylesheet" media="screen" href="css/grid.css" />
<link rel="stylesheet" media="screen" href="css/style.css" />
<link rel="stylesheet" media="screen" href="css/messages.css" />
<link rel="stylesheet" media="screen" href="css/forms.css" />
<link rel="stylesheet" media="screen" href="css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>
<script type="text/javascript" src="js/jquery.tables.js"></script>
<script type="text/javascript" src="js/jquery.flot.js"></script>

<script type="text/javascript" src="js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->
	<script type="text/javascript">
function showUser(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","class_file/show_catagory.php?q="+str,true);
xmlhttp.send();
}
</script>
</head>
<body>
<form class="form3" name="form3" method="post" action="class_file/add_sub_categories_exe.php">
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                    
                  <div style="width:980px;" class="main-content grid_4 alpha">
                      <header>
                        <ul class="action-buttons clearfix fr">
                          <li></li>
                        </ul>
                        <h2>Add Sub category <span style="position:relative; margin-left:170px;"><?php
                                if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
                                foreach($_SESSION['ERRMSG_ARR'] as $msg) {
                                echo $msg; 
                                }
                                unset($_SESSION['ERRMSG_ARR']);
                                }
                                ?></span></h2>
                      </header>
                      <section style="margin-left:300px;" class="clearfix">
                       
                            
                            
							
                            
                            <div class="clearfix">
                                    <label style="position:relative; bottom:10px;"><b>Select category</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    
                                    <select name="select" id="select" style="height:24px; margin:8px;"  onChange="showUser(this.value)">
                                    
                                    <option value="">Select A Category</option>
                                    
                                   <strong>
								   <?php

										
										
										$sql="SELECT * FROM catagory";
										
										$res=mysql_query($sql);
										
										while($row=mysql_fetch_array($res))
										
										{
										
										echo "<option value=".$row['cid'].">".$row['cat_name']."</option>";
										
										}
										
										
										
										?>
                                        </strong>
                                    
                                    </select>
                                </div>
                          <br />
                        	
                        			
                        		
                                
                   	 	
                      </section>
                  </div>
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                        			
                        	
                            <h2> Create A New Sub - Catagory : <span style="position:relative; top:5px;"><input type="text" name="submenu" required maxlength="30" placeholder="create a sub catagory" /><span style="position:relative; top:-6px; margin-left:5px;"><button class="button button-gray" type="submit"><span class="disk"></span>Save</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="button button-gray" type="reset">Reset</button></span></span></h2>
                        </header>
                        <section class="with-table">
                            
                            <div id="txtHint">
<b>
<BR />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please Select A Catagory Sub catagory Will be showed Here
<br><br>
</div>
                            
                            
                            <div class="container_6 clearfix">
<div class="grid_6">
                                    <div class="message info"><b>TIP:</b> You can press CTRL to select multiple rows</div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
              </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
        <?php include('footer.php'); ?>
    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
            <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>
</form>
</body>
</html>
<?php
mysql_close($link);
?>