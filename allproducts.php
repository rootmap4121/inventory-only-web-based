<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
	
	$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("class_file/connection/config.php");	
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('title.php'); ?> 

<link rel="stylesheet" media="screen" href="css/reset.css" />
<link rel="stylesheet" media="screen" href="css/grid.css" />
<link rel="stylesheet" media="screen" href="css/style.css" />
<link rel="stylesheet" media="screen" href="css/messages.css" />
<link rel="stylesheet" media="screen" href="css/forms.css" />
<link rel="stylesheet" media="screen" href="css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>
<script type="text/javascript" src="js/jquery.tables.js"></script>
<script type="text/javascript" src="js/jquery.flot.js"></script>

<script type="text/javascript" src="js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
              <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>All Inventory Product  <span style="position:relative; margin-left:170px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                        </header>
                        <section class="with-table">
                            <table class="datatable selectable full">
                                <thead>
                                    <tr>
                                        <th>Band</th>
                                        <th>Product Name</th>
                                        
                                        <th>Sale Price</th>
                                        <th>Supplier ID</th>
                                        <th>Barcode</th>
                                        <th>Reorder</th>
                                        <th>Stock</th>
                                        <th>Order Due</th>
                                        <th>U. Price</th>
                                        <th>Buying Value</th>
                                        <th>Selling Value</th>
                                  </tr>
                                </thead>
                                
                                
                                <tbody>
                                <?php
								$a=1;
								@$sql_bar=mysql_query("SELECT * FROM `product_barcode` order by pbid asc");
								while($rr=mysql_fetch_array($sql_bar))
								{
									$pbid=$rr['pbid'];
									$band=$rr['brand'];
									$product=$rr['p_name'];
									$sellprice=$rr['price'];
									$reorder=$rr['reorder'];
									$barcode=$rr['barcode'];
									$chk=mysql_num_rows(mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid'"));
										if($chk!=0)
										{
										@$sqlquery=mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid' order by spid DESC LIMIT 1");
										$pdata=mysql_fetch_array($sqlquery);
										$staff_req_id=$pdata['staff_req_id'];
											$sqlsup=mysql_query("SELECT * FROM `order` WHERE staff_req_id='$staff_req_id' LIMIT 1");
											$fetsup=mysql_fetch_array($sqlsup);
												$supplier_id=$fetsup['sup_id'];
										
										
											$sql_qs=mysql_query("SELECT * FROM stockin_product WHERE barcode_id='$pbid'");
											$qs=0;
											$unp=0;
											while($qqs=mysql_fetch_array($sql_qs))
											{
												$qs+=$qqs['quantity'];
												$unp+=$qqs['unite_price'];
											}
											
											//$buyingpriceaverage=intval($unp/$qs);
											$buyingpriceaverage=$unp/$qs;
																			
											$sql_q=mysql_query("SELECT * FROM stock_out WHERE barcode_id='$pbid'");
											$q=0;
											while($qq=mysql_fetch_array($sql_q))
											{
												$q+=$qq['dquantity'];	
											}
										$stock=$qs-$q;
										
										
										
											$sql_qdue=mysql_query("SELECT * FROM `order` WHERE staff_req_id='$staff_req_id' LIMIT 1");
											$qdue=0;
											while($qqdue=mysql_fetch_array($sql_qdue))
											{
												$qdue+=$qqdue['quantity']-$qqdue['dquantity'];	
											}
									
									
									 ?>
                                    <tr>
                                     	<td align="center"><?php echo $band; ?></td>
                                        <td align="center"><?php echo $product; ?></td>
                                        
                                        <td align="center"><?php echo $sellprice; ?></td>
                                        <td align="center"><?php echo $supplier_id; ?></td>
                                        <td align="center"><?php echo $barcode; ?></td>
                                        <td align="center"><?php echo $reorder; ?></td>
                                        <td align="center"><?php echo $stock; ?></td>
                                        <td align="center"><?php echo $qdue; ?></td>
                                        <td align="center"><?php echo $buyingpriceaverage; ?></td>
                                        <td align="center"><?php echo $buyingvalue=$stock*$buyingpriceaverage; ?></td>
                                        <td align="center"><?php echo $bsellvalue=$stock*$sellprice; ?></td>
                                    </tr>
                                    
										<?php 
                                        }
									$a++;
									}  ?>
                                    <tr>
                                     	
                                       
                                        
                                        <td align="center"></td>
                                        <td align="center"></td>
                                        <td align="center"></td>
                                        <td align="center"></td>
                                      <td align="right" colspan="2"><strong>Total : </strong></td>
                                        <td align="center"><?php
										$sql_qst=mysql_query("SELECT * FROM stockin_product");
											$qst=0;
											$qsun=0;
											while($qqst=mysql_fetch_array($sql_qst))
											{
												$qst+=$qqst['quantity'];
												$qsun+=$qqst['unite_price'];
											}
											
																			
											$sql_qt=mysql_query("SELECT * FROM stock_out");
											$qt=0;
											while($qqt=mysql_fetch_array($sql_qt))
											{
												$qt+=$qqt['dquantity'];	
											}
										echo $tstock=$qst-$qt;	
										?></td>
                                        <td align="center">
                                        <?php
										$sql_qduet=mysql_query("SELECT * FROM `order`");
											$qduet=0;
											while($qqduet=mysql_fetch_array($sql_qduet))
											{
												$qduet+=$qqduet['quantity']-$qqduet['dquantity'];	
											}
											echo $qduet;
										?>
                                    </td>
                                        <td align="center"></td>
                                        <td align="center">&nbsp;</td>
                                        <td align="center"></td>
                                    </tr>
                                </tbody>
                            </table>
                          <div class="container_6 clearfix">
  
                            </div>
                        </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
              </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
		<?php include('footer.php'); ?>
    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
            <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>
</body>
</html>