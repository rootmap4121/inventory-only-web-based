<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("class_file/connection/config.php");
	
	$ssid=$_SESSION['SESS_PID'];
	if($ssid==""){
	header("location: add_shop_product.php");
	}	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>The Qube Admin Panel</title> 

<link rel="stylesheet" media="screen" href="css/reset.css" />
<link rel="stylesheet" media="screen" href="css/grid.css" />
<link rel="stylesheet" media="screen" href="css/style.css" />
<link rel="stylesheet" media="screen" href="css/messages.css" />
<link rel="stylesheet" media="screen" href="css/forms.css" />
<link rel="stylesheet" media="screen" href="css/tables.css" />
<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>
<script type="text/javascript" src="js/jquery.tables.js"></script>
<script type="text/javascript" src="js/jquery.flot.js"></script>

<script type="text/javascript" src="js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->
<script type="text/javascript">
function showUser(str)
{
if (str=="")
  {
  document.getElementById("sub_id").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("sub_id").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","class_file/sub_catagory.php?cid="+str,true);
xmlhttp.send();
}
</script>
<!-- Multiple image upload css and javascript code-->

<link href="css/css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
<link href="css/css/fileUploader.css" rel="stylesheet" type="text/css" />

<script src="js/js/jquery-1.6.2.min.js" type="text/javascript"></script>
<script src="js/js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
<script src="js/js/jquery.fileUploader.js" type="text/javascript"></script>


</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                    
                  <div style="width:980px;" class="main-content grid_4 alpha">
                      <header>
                        <h2><a class="modalInput button button-blue">Add Shop Product Image Maximum "5"</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></h2>
                      </header>
                     
                    
                      <section style="margin-left:200px;" class="clearfix">
                      
                                 <div style="height:415px; width:100%; z-index:-1000px;">
                                 <iframe src="class_file/multi_image/index.php" height="95%" width="100%" scrolling="no">
                                 NO Page Found
                                 </iframe>
                                 </div>
                              
                                    <BR>
                                	<div class="action clearfix" align="left">
									  <a class="button button-gray" href="add_shop_product.php"><span class="disk"></span>Finish Step</a>
										<button class="button button-gray" type="reset"  id="buttons">Reset</button>
                       	  </div>
                      
                      </section>
                       </form>
                  </div>
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    
                    <!-- End Tables Section -->
                </section>
              </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
        <?php include('footer.php'); ?>
    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
            <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
