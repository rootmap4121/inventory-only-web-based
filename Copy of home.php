<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>The Qube Admin Panel</title> 

<link rel="stylesheet" media="screen" href="css/reset.css" />
<link rel="stylesheet" media="screen" href="css/grid.css" />
<link rel="stylesheet" media="screen" href="css/style.css" />
<link rel="stylesheet" media="screen" href="css/messages.css" />
<link rel="stylesheet" media="screen" href="css/forms.css" />
<link rel="stylesheet" media="screen" href="css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>
<script type="text/javascript" src="js/jquery.tables.js"></script>
<script type="text/javascript" src="js/jquery.flot.js"></script>

<script type="text/javascript" src="js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">                    
                  
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>Details</h2>
                        </header>
                        <section class="with-table">
                            <table class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Major</th>
                                        <th>Sex</th>
                                        <th>English</th>
                                        <th>Japanese</th>
                                        <th>Calculus</th>
                                        <th>Geometry</th>
                                        <th style="width: 80px"></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Major</th>
                                        <th>Sex</th>
                                        <th>English</th>
                                        <th>Japanese</th>
                                        <th>Calculus</th>
                                        <th>Geometry</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>Student01</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>80</td>
                                        <td>70</td>
                                        <td>75</td>
                                        <td>80</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student02</td>
                                        <td>Mathematics</td>
                                        <td>male</td>
                                        <td>90</td>
                                        <td>88</td>
                                        <td>100</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student03</td>
                                        <td>Languages</td>
                                        <td>female</td>
                                        <td>85</td>
                                        <td>95</td>
                                        <td>80</td>
                                        <td>85</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student04</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>60</td>
                                        <td>55</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student05</td>
                                        <td>Languages</td>
                                        <td>female</td>
                                        <td>68</td>
                                        <td>80</td>
                                        <td>95</td>
                                        <td>80</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student06</td>
                                        <td>Mathematics</td>
                                        <td>male</td>
                                        <td>100</td>
                                        <td>99</td>
                                        <td>100</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student07</td>
                                        <td>Mathematics</td>
                                        <td>male</td>
                                        <td>85</td>
                                        <td>68</td>
                                        <td>90</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student08</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>100</td>
                                        <td>90</td>
                                        <td>90</td>
                                        <td>85</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student09</td>
                                        <td>Mathematics</td>
                                        <td>male</td>
                                        <td>80</td>
                                        <td>50</td>
                                        <td>65</td>
                                        <td>75</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student10</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>85</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student11</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>86</td>
                                        <td>85</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student12</td>
                                        <td>Mathematics</td>
                                        <td>female</td>
                                        <td>100</td>
                                        <td>75</td>
                                        <td>70</td>
                                        <td>85</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student13</td>
                                        <td>Languages</td>
                                        <td>female</td>
                                        <td>100</td>
                                        <td>80</td>
                                        <td>100</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student14</td>
                                        <td>Languages</td>
                                        <td>female</td>
                                        <td>50</td>
                                        <td>45</td>
                                        <td>55</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student15</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>95</td>
                                        <td>35</td>
                                        <td>100</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student16</td>
                                        <td>Languages</td>
                                        <td>female</td>
                                        <td>100</td>
                                        <td>50</td>
                                        <td>30</td>
                                        <td>70</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student17</td>
                                        <td>Languages</td>
                                        <td>female</td>
                                        <td>80</td>
                                        <td>100</td>
                                        <td>55</td>
                                        <td>65</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student18</td>
                                        <td>Mathematics</td>
                                        <td>male</td>
                                        <td>30</td>
                                        <td>49</td>
                                        <td>55</td>
                                        <td>75</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student19</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>68</td>
                                        <td>90</td>
                                        <td>88</td>
                                        <td>70</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student20</td>
                                        <td>Mathematics</td>
                                        <td>male</td>
                                        <td>40</td>
                                        <td>45</td>
                                        <td>40</td>
                                        <td>80</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student21</td>
                                        <td>Languages</td>
                                        <td>male</td>
                                        <td>50</td>
                                        <td>45</td>
                                        <td>100</td>
                                        <td>100</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student22</td>
                                        <td>Mathematics</td>
                                        <td>male</td>
                                        <td>100</td>
                                        <td>99</td>
                                        <td>100</td>
                                        <td>90</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Student23</td>
                                        <td>Languages</td>
                                        <td>female</td>
                                        <td>85</td>
                                        <td>80</td>
                                        <td>80</td>
                                        <td>80</td>
                                        <td>
                                            <a href="#" class="view-details">Details</a>
                                            <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">
                                                    </div>
                                                    <hgroup>
                                                        <h2>John Doe</h2>
                                                        <h6>This is a subtitle. Just something descriptive about this item.</h6>
                                                        <h6>(555) 555-WORK</h6>
                                                    </hgroup>
                                                </header>
                                                <section>
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th></th><th>Column 1</th><th>Column 2</th><th>Column 3</th><th>Column 4</th><th> Column 5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>Item 1</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 2</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 3</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 4</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Item 5</th><td>40</td><td>50</td><td>60</td><td>70</td><td>80</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="container_6 clearfix">
                                <div class="grid_6">
                                    <div class="message info"><b>TIP:</b> You can press CTRL to select multiple rows</div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
              </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
    
    <?php include('footer.php'); ?>


    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
            <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
