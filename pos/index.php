<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
	$fulldate=date('d/m/Y');
		$status=$_SESSION['SESS_STATUS'];
		
		
		//session_cache_limiter(1000);
			include("../class_file/connection/config.php");
				
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php
include('../title.php');
?>

<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
              <?php
			  
			  if($status==2){
			  

			    ?>
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                    <div class="main-content">
                      <section class="with-table">
                        <div class="container_6 clearfix">
                            <div class="grid_4 clearfix">
                                    <header class="clearfix">
                                   <br>
                                    <h3>Point of Sales Statistics</h3>
                                    </header>
                              <section>
                                    <div class="grid_1 alpha">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Total Sales</h2></header>
                                            <section><h1><?php 
						echo mysql_num_rows(mysql_query("SELECT * FROM stockin_product WHERE spdate='$fulldate'"));  
											?></h1></section>
                                        </div>
                                    </div>
                                    <div class="grid_1">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Sales Exchange</h2></header>
                                            <section><h1>
                                            <? 
											echo mysql_num_rows(mysql_query("SELECT * FROM stock_out WHERE stockoutdate='$fulldate'"));
											?>
                                            </h1></section>
                                        </div>
                                    </div>
                                <div class="grid_1">
                                        <div class="widget black ac">
                                            <header>
                                              <h2>Total Re-Order</h2></header>
                                            <section><h1>
                                            <?php
											echo mysql_num_rows(mysql_query("SELECT * FROM `staff_requisation` WHERE srdate='$fulldate'"));
											?>
                                            </h1></section>
                                        </div>
                                </div>
                                    </section>
                          </div>
                                <br>
                        </div>
                      </section>
                  </div>
              <div class="clear"></div>

                    <!-- Tables Section -->
                  <!-- End Tables Section -->
                </section>
                <?php
				}
				
				?>
                
            </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
    
    <?php include('../footer.php'); ?>


    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
          <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
