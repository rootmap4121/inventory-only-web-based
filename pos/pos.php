<?php	
session_start();
//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) 
	{

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;
			//If there are input validations, redirect back to the login form
			if($errflag){
						$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
						session_write_close();
						header("location: index.php");
						exit();
						}
	}
	$status=$_SESSION['SESS_STATUS'];
	//session_cache_limiter(1000);
	include("../class_file/connection/config.php");
	extract($_GET);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('../title.php'); ?>
<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->
<script>
function showUser(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","pom.php?q="+str,true);
xmlhttp.send();
}
</script>
<script src="ajax_framework.js" language="javascript"></script>
<script src="ajax_framework_process_order.js" language="javascript"></script>
<style type="text/css">
.form_style #contentText {
	border: 1px solid #CCCCCC;
	border-radius: 5px;
}
.form_style #FormSubmit {
	display: block;
	background: #003366;
	border: 1px solid #000066;
	color: #FFFFFF;
	margin-top: 5px;
}
#responds{
	margin: 0px;
	padding: 0px;
	list-style: none;
}
#responds li{
	list-style: none;
	padding: 10px;
	background: rgb(182, 232, 255);
	margin-bottom: 5px;
	border-radius: 5px;
	max-width: 400px;
	font-family: arial;
	font-size: 13px;
}
.del_wrapper{float:right;}
.content_wrapper {
	max-width: 400px;
	margin-right: auto;
	margin-left: auto;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	//##### Add record when Add Record Button is click #########
	$("#FormSubmit").click(function (e) {
			e.preventDefault();
			if($("#contentText").val()==='')
			{
				alert("Please enter some text!");
				return false;
			}
		 	//var myData = 'content_txt='+ $("#contentText").val();
			var myData=$('#contentText').attr('value');
			var randomcart =$("#randomcart").attr('value');
			//build a post data structure
			jQuery.ajax({
			type: "POST", // Post / Get method
			url: "response.php", //Where form data is sent on submission
			dataType:"text", // Data type, HTML, json etc.
			//data:myData, //Form variables
			data: "content_txt="+ myData+"&randomcart="+randomcart,
			cache: false,
			success:function(response){
				$("#responds").append(response);
				$("#contentText").val('Success'); //empty text field after successful submission
			},
			error:function (xhr, ajaxOptions, thrownError){
				alert(thrownError);
			}
			});
	});

	//##### Delete record when delete Button is click #########
	$("body").on("click", "#responds .del_button", function(e) {
		 e.returnValue = false;
		 var clickedID = this.id.split('-'); //Split string (Split works as PHP explode)
		 var DbNumberID = clickedID[1]; //and get number from array
		 var myData = 'recordToDelete='+ DbNumberID; //build a post data structure
		 
			jQuery.ajax({
			type: "POST", // Post / Get method
			url: "response.php", //Where form data is sent on submission
			dataType:"text", // Data type, HTML, json etc.
			data:myData, //Form variables
			success:function(response){
				//If we get success response hide content user wants to delete.
				$('#item_'+DbNumberID).fadeOut("slow");
			},
			error:function (xhr, ajaxOptions, thrownError){
				//On error, we alert user
				alert(thrownError);
			}
			});
	});
});
</script>
</head>
<body>
    <div id="wrapper">
        <?php //include('header_button.php'); ?>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <!--<div style="padding-top:-102px;">
              <div>-->
              <?php
			  if($status==2){ ?>
                <section class="main-section grid_4">
                    <!-- Formshttp://livedemo00.template-help.com/wt_43328/ Section --><!-- End Forms Section -->
                    
                  <div class="main-content grid_4 alpha">
                      <header>
                      
                        <h2>Point of Sales</h2>
                      </header>
                      <section class="clearfix">
              <form class="form" method="post" action="index.php">
                         			<div class="clearfix">
                                    	<label>Invoice Number <em>*</em><small>Type a Unique Number </small>
                                        </label><input type="text" name="content_txt" id="contentText" required />
                                        <input name="randomcart" id="randomcart" type="hidden" value="<?php echo $cardid="Card".time()*123; ?>">
                                    </div>
                                   
                                	<div class="clearfix">
                                    	<label>Customer Id <em>*</em><small>max. 30 char.</small></label>
                                        <input type="text" name="cid" id="cid" class="full small smalls" />
                                        <input type="submit" class="button button-gray full small" id="FormSubmit"  value="Add New" />
                                	</div>
                                    <div class="clearfix">
                                    	<label>Customer Name <em>*</em><small>A valid Name</small></label>
                                        <input type="text" name="cname" id="cname" required />
                                        </div>
                                    <div class="clearfix">
                                        <label>Product SNL <em>*</em><small>Barcode Number</small></label>
                                        <input type="text" name="snl" id="snl"  required />
                                    </div>
                                    <div class="clearfix">
                                        <label>Item <em>*</em><small>A valid email address</small></label>
                                        <input type="text" name="item" id="item" required />
                                    </div>
                                    <div class="clearfix">
                                        <label>Quantity <em>*</em><small>A valid email address</small></label>
                                        <input type="text" name="quantity" id="quantity" required />
                                    </div>
                                    <div class="clearfix">
                                        <label>Unit Price <em>*</em><small>A valid email address</small></label>
                                        <input type="text" name="uniteprice" id="uniteprice" required />
                                    </div>
                                    <div class="clearfix">
                                        <label>Sub Total<em>*</em><small>A valid email address</small></label>
                                        <input type="text" name="subtotal" id="subtotal" required />
                                    </div>
                                    <div class="clearfix">
                                    	<label>Total Price <em>*</em><small>Alphanumeric(max 12 char.)</small></label>
                                        <input type="text" name="totalprice" id="totalpeice" required />
                                	</div>
                                    
                            </form>
                      </section>
                  </div>
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <!-- End Tables Section -->
                </section>
                
                
                
                
                
                
                
                <section class="main-section grid_4" style="margin-left:-1px;">
                    <!-- Forms Section --><!-- End Forms Section -->
                    
                  <div class="main-content grid_4 omega">
                          <header>
                                <h2>Point of Sales</h2>
                          </header>
                      <section class="clearfix">
             				 <form class="form" method="post" action="index.php">
                             <div class="clearfix">
                              			<label>Vat<em>*</em><small>Alphanumeric(max 12 char.)</small></label>																							                                        <input type="text" name="p_name" class="full small smalls" id="p_name" required />
                                        <input type="hidden" name="cartid" value="<?php echo $cardid; ?>">      
                                	</div>
                                	<div class="clearfix">
                                    	<label>Net Price <em>*</em><small>max. 30 char.</small></label>
                                        <input type="text" name="quantity" id="quantity" />
                                	</div>
                         			<div class="clearfix">
                                    	<label>Paid Amount <em>*</em><small>A valid email address</small></label>
                                        <input type="text" name="uniteprice" id="uniteprice"  required />
                                    </div>
                                    <div class="clearfix">
                                        <label>Due Amount <em>*</em><small>A valid email address</small></label>
                                        <input type="text" name="totalprice" id="totalprice" required />
                                    </div>
                                    <div class="action clearfix">
										<button type="submit" name="submit" class="button button-green">Save </button>
                                	</div>
                               </form>
                      </section>
                  </div>
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <!-- End Tables Section -->
                </section>
                
                <section class="main-section grid_4" style="margin-left:-1px;">
                    <!-- Forms Section --><!-- End Forms Section -->
                    
                  <div class="main-content grid_4 omega">
                          <header>
                                <h2>Report</h2>
                          </header>
                      <section class="with-table">
                            <table class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th width="55">SNL</th>
                                      <th width="140" align="center">Quantity</th>
                                        <th width="124" align="center">Unite Price</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>SNL</th>
                                        <th>T.Q. : </th>
                                        <th>T.P : </th>
                                    </tr>
                                </tfoot>
                                
                                <tbody id="responds">
                               		<?php
										//include db configuration file
										//include_once("config.php");
										
										//MySQL query
										$Result = mysql_query("SELECT inid,cid FROM invoice");
										
										//get all records from add_delete_record table
										while($row = mysql_fetch_array($Result))
										{
									?>
                                      <tr  id="item_<?php echo $row['inid']; ?>">
                                    	<td align="center">1</td>
                                        <td align="center">1</td>
                                        <td align="center"><?php echo $row["cid"]; ?></td>
                                    </tr>
                                    
                                    <?php 
										}
									?>
                                </tbody>
                            </table>
                          <div class="container_6 clearfix">
  
                            </div>
                      </section>
                  </div>
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <!-- End Tables Section -->
                </section>
                <?php 
				}
				?>
            <!--</div>

                <!-- Main Section End 

            </div>-->
    </div>
        <?php include('../footer.php'); ?>
    <!-- simple dialog -->
    
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.7
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
