<?php
//include db configuration file
include_once("../class_file/connection/config.php");

if(isset($_POST["content_txt"]) && strlen($_POST["content_txt"])>0) 
{	//continue only if POST value content_txt is filled by user

	/* 
	sanitize post value, PHP filter FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH
	Strip tags, encode special characters.
	*/
	$card_id=filter_var($_POST['randomcart'],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
	$contentToSave = filter_var($_POST["content_txt"],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH); 
	
	// Insert sanitize string in record
	if(mysql_query("INSERT INTO invoice(cid,cartid) VALUES('".$contentToSave."','".$card_id."')"))
	{
		 //Record is successfully inserted, respond result to ajax request
		  $my_id = mysql_insert_id(); //Get ID of last inserted content from MySQL
		  ?>
                                      <tr  id="item_<?php echo $my_id; ?>">
                                    	<td align="center">1</td>
                                        <td align="center"><?php echo $card_id; ?></td>
                                        <td align="center"><?php echo $contentToSave; ?></td>
                                    </tr>
                                    <?php
		  

	}else{
		//output error
		
		/*
		header('HTTP/1.1 500 '.mysql_error()); //display sql errors.. must not output sql errors in live mode.
		*/

		header('HTTP/1.1 500 Looks like mysql error, could not insert record!');
		exit();
	}

}
elseif(isset($_POST["recordToDelete"]) && strlen($_POST["recordToDelete"])>0 && is_numeric($_POST["recordToDelete"]))
{	//continue only if POST value "recordToDelete" is available and it's numeric

	/* 
	sanitize post value, PHP filter FILTER_SANITIZE_NUMBER_INT 
	removes all characters except digits, plus and minus sign.
	*/
	$idToDelete = filter_var($_POST["recordToDelete"],FILTER_SANITIZE_NUMBER_INT); 
	
	//try deleting record using the record ID we received from POST
	if(!mysql_query("DELETE FROM invoice WHERE inid=".$idToDelete))
	{    
		//If mysql delete redord is unsuccessful out put error 
		header('HTTP/1.1 500 Could not delete record!');
		exit();
	}
}
else
{
	//Output error
	header('HTTP/1.1 500 Error occurred, Could not process request!');
    exit();
}
//mysql_close($connecDB);
?>