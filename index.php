<?php 

session_start();

@$chk=$_SESSION['SESS_ID'];
			include('class_file/connection/config.php');			

if($chk==!""){ header('location: home.php');
	}else{
		 ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>The Qube Admin Panel</title> 

<link rel="stylesheet" media="screen" href="css/reset.css" />
<link rel="stylesheet" media="screen" href="css/grid.css" />
<link rel="stylesheet" media="screen" href="css/style.css" />
<link rel="stylesheet" media="screen" href="css/messages.css" />
<link rel="stylesheet" media="screen" href="css/forms.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/PIE.js"></script>
<script src="js/IE9.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>

<script type="text/javascript" src="js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

<script> 
$(document).ready(function(){
    $.tools.validator.fn("#username", function(input, value) {
        return value!='Username' ? true : {     
            en: "Please complete this mandatory field"
        };
    });
    
    $.tools.validator.fn("#password", function(input, value) {
        return value!='Password' ? true : {     
            en: "Please complete this mandatory field"
        };
    });

    var form = $("#form").validator({ 
    	position: 'bottom left', 
    	offset: [5, 0],
    	messageClass:'form-error',
    	message: '<div><em/></div>' // em element is the arrow
    }).attr('novalidate', 'novalidate');
});
</script> 


</head>
<body class="login">
    <div class="login-box main-content">
      <header>
          <ul class="action-buttons clearfix fr">
              <li><a href="#" class="button button-gray"><span class="help"></span>Forgot Password</a></li>
          </ul>
        <h2>Login</h2>
      </header>
    	<section>
    		<div class="message notice">Enter any letter and press Login                                 <?php 
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			
			echo "<br>".$msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></div>
    		<form id="form" action="class_file/login_exe.php" method="post" class="clearfix">
			<p>
				<input type="text" id="username"  class="large" value="" name="username" required="required" placeholder="Username" />
                        <input type="password" id="password" class="large" value="" name="password" required="required" placeholder="Password" />
                        
                        <input type="submit" class="large button button-gray fr" value="Login">
                        
			</p>
			<p class="clearfix">
				<span class="fl">
					<input type="checkbox" id="remember" class="" value="1" name="remember"/>
					<label class="choice" for="remember">Keep me logged-in for two weeks</label>
				</span>
			</p>
		</form>
    	</section>
    </div>
</body>
</html>
<?php } ?>