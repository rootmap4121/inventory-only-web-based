<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("class_file/connection/config.php");	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>The Qube Admin Panel</title> 

<link rel="stylesheet" media="screen" href="css/reset.css" />
<link rel="stylesheet" media="screen" href="css/grid.css" />
<link rel="stylesheet" media="screen" href="css/style.css" />
<link rel="stylesheet" media="screen" href="css/messages.css" />
<link rel="stylesheet" media="screen" href="css/forms.css" />
<link rel="stylesheet" media="screen" href="css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.ui.min.js"></script>
<script type="text/javascript" src="js/jquery.tables.js"></script>
<script type="text/javascript" src="js/jquery.flot.js"></script>

<script type="text/javascript" src="js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
              <?php if($status==1){ ?>
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>Total Mail  <span style="position:relative; margin-left:10px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                        </header>
                        <section class="with-table">
                            <table align="center" class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th width="76">ID</th>
                                      <th width="227">From</th>
                                      <th width="184">Subject</th>
                                      <th width="214">Email</th>
                                      <th colspan="2"><a href="#" class="button button-gray view-details">Delete All</a></th>
                                  </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>From</th>
                                        <th>Subject</th>
                                        <th>Email</th>
                                        <th>&nbsp;</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php
								$a=1;
								$sqlquery=@mysql_query("SELECT * FROM mail order by id asc");
								while($alldata=mysql_fetch_array($sqlquery)){
								?>
                                    <tr>
                                        <td align="center"><?php echo $a; ?></td>
                                        <td align="center"><?php
														
														$usr=$alldata['usr'];
														if($usr==3){
														echo "Customer";
														}
														elseif($usr==2){
														echo "Shop Admin";
														}
														
														 ?></td>
                                        <td align="center"><?php echo $alldata['subject']; ?></td>
                                        <td align="center"><?php echo $alldata['email'];	?></td>
                                        <td width="113"  align="center"><a href="#" class="button button-gray view-details">Details</a>
                                        <div class="overlay-details">
                                                <header class="clearfix">
                                                    <div class="avatar fl">
                                                        <img src="images/woofunction-icons/user_32.png" width="50" height="50">                                                    </div>
                                                    <hgroup>
                                                        <h2>Mail From :<?php echo $alldata['email']; ?></h2>
                                                        <h6>Subject : <?php echo $alldata['subject']; ?></h6>
                                                        <h6>User Type : <?php 
														if($usr==3){
														echo "Customer";
														}
														elseif($usr==2){
														echo "Shop Admin";
														}
														
														 ?></h6>

                                                    </hgroup>
                                                </header>
                                                <section>
                                                <form action="class_file/message_admin_reply.php" method="post">
                                                    <table class="simple full">
                                                        <thead>
                                                            <tr>
                                                                <th width="30%">Message : </th><th><?php echo $alldata['message'];  ?></th>
                                                            </tr>
                                                            <tr>
                                                                <th>Reply</th><th><input name="rurl" type="hidden" value="all_admin_new_mail.php"><input name="id" type="hidden" value="<?php echo $alldata['id']; ?>"></th>
                                                            </tr>
                                                            <tr>
                                                                <th></th><th><textarea name="reply" cols="30" rows="5"><?php echo $alldata['reply'];  ?></textarea></th>
                                                            </tr>
                                                              <tr>
                                                                <th></th><th><input type="submit" class="button button-gray" name="reply" value="Reply Now"></th>
                                                            </tr>
                                                        
                                                        </thead>
                                                    </table>
                                                    </form>
                                                </section>
                                            </div>                                        </td>
                                        <td width="138" align="center">
                                            <form action="class_file/delete_customer.php" method="post"><input name="rurl" type="hidden" value="all_admin_new_mail.php"><input name="id" type="hidden" value="<?php echo $alldata['id']; ?>"><input name="username" type="hidden" value="<?php echo $alldata['username'];  ?>"><button class="button button-gray" type="submit">Delete</button></form>                                                                          </td>
                                    </tr>
                                    <?php
									$a++;
									}
									
									 ?>
                                </tbody>
                            </table>
<div class="container_6 clearfix">
                                <div class="grid_6">
                                    <div class="message info"><b>TIP:</b> You can press CTRL to select multiple rows</div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
                <?php
				}
				elseif($status==2){
				?>
				                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>Compose Mail  <span style="position:relative; margin-left:10px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                        </header>
                        <section class="with-table">
                          <form name="form1" method="post" action="class_file/send_internal_mail.php">
                          
                           <table align="center" cellpadding="0" cellspacing="0" border="0" style="width:500px; position:relative; left:300px;">
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">To :</td>
                           <td align="left" valign="top"><input type="text" name="textfield" value="<?php
						   echo "Sales Collector Admin"; ?>" id="textfield" readonly="readonly" style="width:250px;">
                           <input name="c_s_id" type="hidden" value="<?php echo $sid; ?>">
                           </td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">Form :</td>
                           <td align="left" valign="top"><input type="text" name="from" value="<?php
						   $getshop=mysql_fetch_array(mysql_query("SELECT fname,email,status FROM signup WHERE id='$sid'"));
						   
						   
						   
						    echo $getshop['fname']; ?>" id="from"  readonly="readonly"  style="width:250px;"></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;<input name="email" type="hidden" value="<?php echo $getshop['email']; ?>"></td>
                           <td align="left" valign="top">&nbsp;<input name="status" type="hidden" value="0"></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">Subject :</td>
                           <td align="left" valign="top"><input type="text" name="subject" id="subject" style="width:250px;"></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;<input name="usr" type="hidden" value="<?php echo $getshop['status']; ?>"></td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
                           <tr>
                           <td align="left" valign="top"><span  style="position:relative; top:-60px;">Message :</span></td>
                           <td align="left" valign="top"><textarea name="message" id="message" cols="45" rows="5" style="width:250px;"></textarea></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="center" valign="top"><span style="position:relative; right:10px;"><input type="submit" class="button button-gray view-details" name="button" id="button" value="Submit" style="height:30px; cursor:pointer;">&nbsp;&nbsp;<input type="reset"  class="button button-gray view-details"  name="reset" id="reset" value="Reset" style="height:30px; cursor:pointer;"></span></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
							</table>
                            </form>
<div class="container_6 clearfix">
                                <div class="grid_6">
                                    <div class="message info"><b>TIP:</b> You can press CTRL to select multiple rows</div>
                                </div>
                          </div>
                        </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
				<?php
				}
				elseif($status==3){
				?>
				                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>Compose Mail  <span style="position:relative; margin-left:10px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                        </header>
                        <section class="with-table">
                          <form name="form1" method="post" action="class_file/send_internal_mail.php">
                          
                           <table align="center" cellpadding="0" cellspacing="0" border="0" style="width:500px; position:relative; left:300px;">
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">To :</td>
                           <td align="left" valign="top"><input type="text" name="textfield" value="<?php
						   echo "Sales Collector Admin"; ?>" id="textfield" readonly="readonly" style="width:250px;">
                           <input name="c_s_id" type="hidden" value="<?php echo $sid; ?>">
                           </td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">Form :</td>
                           <td align="left" valign="top"><input type="text" name="from" value="<?php
						   $getshop=mysql_fetch_array(mysql_query("SELECT fname,email,status FROM signup WHERE id='$sid'"));
						   
						   
						   
						    echo $getshop['fname']; ?>" id="from"  readonly="readonly"  style="width:250px;"></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;<input name="email" type="hidden" value="<?php echo $getshop['email']; ?>"></td>
                           <td align="left" valign="top">&nbsp;<input name="status" type="hidden" value="0"></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">Subject :</td>
                           <td align="left" valign="top"><input type="text" name="subject" id="subject" style="width:250px;"></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;<input name="usr" type="hidden" value="<?php echo $getshop['status']; ?>"></td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
                           <tr>
                           <td align="left" valign="top"><span  style="position:relative; top:-60px;">Message :</span></td>
                           <td align="left" valign="top"><textarea name="message" id="message" cols="45" rows="5" style="width:250px;"></textarea></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="center" valign="top"><span style="position:relative; right:10px;"><input type="submit" class="button button-gray view-details" name="button" id="button" value="Submit" style="height:30px; cursor:pointer;">&nbsp;&nbsp;<input type="reset"  class="button button-gray view-details"  name="reset" id="reset" value="Reset" style="height:30px; cursor:pointer;"></span></td>
                           </tr>
                           <tr>
                           <td align="left" valign="top">&nbsp;</td>
                           <td align="left" valign="top">&nbsp;</td>
                           </tr>
							</table>
                            </form>
<div class="container_6 clearfix">
                                <div class="grid_6">
                                    <div class="message info"><b>TIP:</b> You can press CTRL to select multiple rows</div>
                                </div>
                          </div>
                        </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
				<?php
				}
                ?>
              </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
        <?php include('footer.php'); ?>
    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
            <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
