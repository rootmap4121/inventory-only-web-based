<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) 
	{

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) 
						{
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}
	}
	 
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("../class_file/connection/config.php");
	extract($_GET);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('../title.php'); ?>
<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->
<script>
function showUser(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","pom.php?q="+str,true);
xmlhttp.send();
}
</script>
<script src="ajax_framework.js" language="javascript"></script>
<script src="ajax_framework_process_order.js" language="javascript"></script>
</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
              <?php
			  if($status==2){ ?>
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                    
                  <div style="width:980px;" class="main-content grid_4 alpha">
                      <header>
                      
                        <h2>Order Delivery Record / <a href="placed_order_product_delivery_payment_history.php?oid=<?php echo $oid; ?>">Payment HIstory</a> <span id="insert_response" style="position:relative; margin-left:10px; font:Arial, Helvetica, sans-serif; color:#F00; text-decoration:blink;"></span><span style="position:relative; margin-left:10px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                      </header>
                      <section style="margin-left:200px;" class="clearfix">
              <form class="form" method="post" action="javascript:process_order()">
                         			<div class="clearfix">
                                    	<label>Product Name <em>*</em><small>Alphanumeric(max 12 char.)</small></label><input type="text" name="p_name" id="p_name" value="<?php echo $p_name; ?>" required />
                                        <input name="pid" id="pid" type="hidden" value="<?php echo @$id; ?>">										<input name="access" id="access" type="hidden" value="<?php echo $_SESSION['SESS_ID']; ?>">
                                	</div>
                                    
                                	<div class="clearfix">
                              <label>Supplier Name<em>*</em><small>max. 30 char.</small></label>																							                                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                	<B><?php echo $sip; ?></B>
                                        
                                        
                                	</div>
                                    <br>
                                	<div class="clearfix">
                                    	<label>Quantity <em>*</em><small>max. 30 char.</small></label><input type="text" name="quantity" id="quantity"  value="<?php echo $quantity; ?>" />
                                	</div>
                                    <div class="clearfix">
                                    	<label>Unite Price <em>*</em><small>A valid email address</small></label><input type="text" name="uniteprice" id="uniteprice"  value="<?php echo $uniteprice; ?>"  required />
                                        </div>
                                        <div class="clearfix">
                                    	<label>Total Price <em>*</em><small>A valid email address</small></label><input type="text" name="totalprice" id="totalprice"  value="<?php echo $totalprice; ?>" required />
                                        </div>
                                        <div class="clearfix">
                                    	<label>Advance Payment <em>*</em><small>A valid email address</small></label><input type="text"   value="<?php echo $advancepayment; ?>"   name="advancepayment" id="advancepayment" required />
                                        </div>
                                        <div class="clearfix">
                                    	<label>Due <em>*</em><small>A valid email address</small></label><input type="text" name="due" id="due" value="<?php echo $due; ?>" required />
                                        </div>
                                         <div class="clearfix">
                                    	<label>Payment Mode <em>*</em><small>A valid email address</small></label>
                                        <input type="text" name="pom" id="pom" value="<?php echo $pom; ?>" required />
                                        </div>
                                        <div class="clearfix">
                                    	<label>Delivery Date <em>*</em><small>A valid email address</small></label><input type="date" value="<?php echo $d_date; ?>" name="deliverydate" id="deliverydate" required />
                                        </div>
                                      
                                	
                                <div class="clearfix">
                                    	<label>Order Place Date <em>*</em></label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                	<B><?php echo $o_date; ?></B>
                                    </div>
                                    <BR>
                                	<div class="action clearfix" align="left">
										<ul class="action-buttons clearfix">
                          <li><a class="modalInput button button-blue" rel="#dialog">Complete Delivery Process</a></li>
                        </ul>
                                	</div>
                            </form>
                      </section>
                  </div>
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <!-- End Tables Section -->
                </section>
                <?php 
				}
				?>
            </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
        <?php include('../footer.php'); ?>
    <!-- simple dialog -->
    <div class="widget modal" id="dialog">
        <header><h2>Please Copmple Supplier Delivery</h2></header>
        <section>
        	<form method="post" action="delivery_order.php">
            <p>
       &nbsp;&nbsp; <input type="text" name="p_name" id="p_name" value="<?php echo $p_name; ?>" required /> Product Name *
    <input name="oid" type="hidden" value="<?php echo $oid; ?>">
    
            </p>
            <p>
            &nbsp;&nbsp; <input type="text" name="quantity" id="quantity"  value="<?php echo $quantity; ?>" /> Quantity *
            </p>
            <p>
            &nbsp;&nbsp;
            <?php
			
			$sql_dq=mysql_query("SELECT * FROM `order` WHERE oid='$oid'");
			$fet_dq=mysql_fetch_array($sql_dq);
			?> 
            <input type="text" class="half" onKeyUp="this.form.duequantity.value = ((this.form.extradue.value-this.form.dquantity.value))"  name="dquantity" id="dquantity"  value="<?php echo  $fet_dq['dquantity']; ?>"   required />Deli. Quantity*&nbsp;&nbsp;
            <input name="extradue" type="hidden" value="<?php echo  $fet_dq['quantity']-$fet_dq['dquantity']; ?>">

            &nbsp;&nbsp; <input type="text" class="half" name="duequantity" id="duequantity"  value="<?php echo $fet_dq['duequantity']; ?>"   required />Due Quantity*
            </p>
            <p>
            $ <input type="text" class="half" name="uniteprice" id="uniteprice"  value="<?php echo $uniteprice; ?>"  required /> Unite Price*
            &nbsp;&nbsp;&nbsp;&nbsp;
            $ <input type="text" class="halfm" name="totalprice" id="totalprice"  value="<?php echo $totalprice; ?>" required />Total Price*
            </p>
            <p>
            $ <input type="text" class="halfm"   value="<?php echo $advancepayment; ?>"   name="advancepayment" id="advancepayment" required /> Advance*
            
            $ <input type="text" name="due" class="halfm" id="due" value="<?php echo $due; ?>" required /> Due*
            </p>
            <p>
            
            </p>
            <p>
            &nbsp;&nbsp; <select name="pom">
            <option value="">  Select Option  </option>
            <option value="1">Cash</option>
            <option value="2">Cheque</option>
            </select> Payment Option
            </p>
            <p>
            
            </p>
            <p>
                &nbsp;&nbsp; <input type="text" name="cash" id="cash" required /> Amount
    </p>
    <p>	&nbsp;&nbsp; <input type="text" name="checquename" id="checquename" /> Checque Number
    </p>
    <p>
    &nbsp;&nbsp; <input type="text" name="bankname" id="bankname" /> Bank Name

    </p>
    <p>
    <?php 
	
	
	$sqlss=mysql_query("SELECT * FROM `order` WHERE oid='$oid'");
	$fetss=mysql_fetch_array($sqlss);
	
	?>
    <input name="ccc" type="hidden"  value="<?php echo $fetss['totalpay']; ?>" >
    &nbsp;&nbsp; <input type="text" name="sdfsdfsf" id="sdfsdfsf" value="<?php echo $fetss['totalpay']; ?>" required /> Total Paid
    </p>
          
            
            
            <p>
                <input type="submit" name="Submit" value="Insert" class="button button-green"/>
                <button type="button" class="button button-red close" onClick="refresh()">Close</button>
            </p>
            </form>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.7
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
