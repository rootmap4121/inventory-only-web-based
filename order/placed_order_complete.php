<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("../class_file/connection/config.php");	
	$access=$_SESSION['SESS_ID'];
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('../title.php'); ?>

<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        <?php
			  $usr=$_SESSION['SESS_USERNAME'];

				@$sql_check_tab=mysql_num_rows(mysql_query("SELECT * FROM system_admin WHERE username='$usr'"));
				
				
				if($sql_check_tab!=0)
				{
			  ?>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
              <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>All Placed Order ( <?php echo mysql_num_rows(mysql_query("SELECT * FROM `order` WHERE status='1'")); ?> )  <span style="position:relative; margin-left:170px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                        </header>
                        <section class="with-table">
                            <table class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th width="61">ID</th>
                                      <th width="135" align="center">Supplier Name</th>
                                        <th width="136" align="center">Product</th>
                                        <th width="95">Quantity</th>
                                        <th width="121">Total Price</th>
                                      <th width="120">Unite Price</th>
                                      <th width="98">O.Date</th>
                                      <th width="87">D.Date</th>
                                      <th>Status</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Supplier Name</th>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>
                                        <th>Unite Price</th>
                                        <th>O.Date</th>
                                        <th>D.Date</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                                
                                <tbody>
                                <?php
								$a=1;
								@$sqlquery=mysql_query("SELECT * FROM `order` WHERE status='1' order by oid asc");
								while($pdata=mysql_fetch_array($sqlquery)){
								
								 ?>
                                    <tr>
                                     	<td align="center"><?php echo $a;  ?></td>
                                        <td align="center"><?php $sid=$pdata['sup_id'];
										$sql_ss=mysql_query("SELECT * FROM supplier WHERE sup_id='$sid'");
										$fet_ss=mysql_fetch_array($sql_ss);
										echo $fet_ss['f_name'];
										?></td>
                                        <td align="center"><?php echo  $pdata['p_name']; ?></td>
                                        <td align="center">
                                        <?php
										echo $pdata['quantity'];
										?></td>
                                        <td align="center">$<?php
										 echo $pdata['total_price'];
										
										?>
                                                                                                 
										</td>
                                        
                                      <td align="center">
                                      $
                                     
                                        <?php echo $pdata['unite_price']; ?>
                                      

                                      </td>
                                      <td align="center"><?php echo $pdata['o_date']; ?></td>
                                      <td align="center"><?php echo $pdata['d_date']; ?></td>
                                      <td width="87" align="center">
                                      <?php
									  $stt=$pdata['status'];
									  if($stt==0)
									  {
									  ?>
                                      <a href="#" class="button button-orange">Pending</a>
                                      <?php
									  }
									  elseif($stt==1)
									  {
									  ?>
                                      <a href="#" class="button button-green">Complete</a>
                                      <?php
									  }
									  ?>
                                      </td>
                                    </tr>
                                    
                                    <?php  
									$a++;
									}  ?>
                                </tbody>
                            </table>
                          <div class="container_6 clearfix">
  
                            </div>
                      </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
            </div>

                <!-- Main Section End -->

            </div>
        </section>
        <?php
		}
		
		?>
    </div>
    
		<?php include('../footer.php'); ?>
    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>This is a simple modal dialog</h2></header>
        <section>
          <p>
                Are you sure you want to do this?
            </p>

            <!-- yes/no buttons -->
            <p>
                <button class="button button-blue close">Yes</button>
                <button class="button button-gray close">No</button>
            </p>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
