<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) 
	{

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) 
						{
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}
	}
	 
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("../class_file/connection/config.php");
	extract($_GET);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('../title.php'); ?>
<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->
<script>
function showUser(str)
{
if (str=="")
  {
  document.getElementById("txtHint").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","../class_file/checkuser.php?q="+str,true);
xmlhttp.send();
}
</script>
<script src="ajax_framework.js" language="javascript"></script>
</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
              <?php
			  if($status==2){ ?>
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
                    
                  <div style="width:980px;" class="main-content grid_4 alpha">
                      <header>
                      <ul class="action-buttons clearfix fr">
                          <li><a class="modalInput button button-blue" rel="#simpledialog">Add New Supplier</a></li>
                        </ul>
                        <h2>Product Stock In <span id="insert_response" style="position:relative; margin-left:10px; font:Arial, Helvetica, sans-serif; color:#F00; text-decoration:blink;"></span><span style="position:relative; margin-left:10px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                      </header>
                      <section style="margin-left:200px;" class="clearfix">
              <form class="form" method="post" action="javascript:insert()">
                         			<div class="clearfix">
                                    	<label>Select Barcode <em>*</em><small>max. 30 char.</small></label>
                                         <select name="barcode_id" id="barcode_id" required>
                                 				<option selected>Chose Option</option>
                                                <?php
											$sqlb=mysql_query("SELECT * FROM product_barcode");
												while($rr=mysql_fetch_array($sqlb))
												{
													?>
                                                <option value="<?php echo $rr['pbid']; ?>"><?php echo $rr['barcode']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rr['p_name']; ?></option>
                                                <?php
												}
												?>
                                                
                                                </select>
                                                <input name="stockin_id" id="stockin_id" type="hidden" value="<?php echo $stockin_id; ?>">
                                	</div>
                                    <div class="clearfix">
                                    	<label>Product Name <em>*</em><small>&nbsp;</small></label>
                                        <input name="p_name" readonly type="text" value="<?php echo @$p_name; ?>">
                                        </div>
                                        <!--<div class="clearfix">
                                    	<label>User Product Name <em>*</em><small>&nbsp;</small></label>
                                        <input name="pp" type="text" value="<?php //echo @$uniteprice; ?>">
                                        </div>-->
                                        <div class="clearfix">
                                    	<label>Quantity <em>*</em><small>A valid email address</small></label><input type="text" name="quantity" readonly id="quantity" value="<?php echo $quantity; ?>"  required />
                                        </div>
                                        
                                        <div class="clearfix">
                                    	<label>StockIn Date <em>*</em><small>A valid email address</small></label><input type="date"  name="stockindate" id="stockindate" value="<?php echo $date=date('d/m/Y'); ?>" required />
                                        </div>
                                      
                                	
                                	<div class="action clearfix" align="left">
										<button class="button button-gray" type="submit"><span class="disk"></span>Save</button>
                                    	<button class="button button-gray" type="reset">Reset</button>
                                	</div>
                            </form>
                      </section>
                  </div>
                  <div class="clear"></div>

                    <!-- Tables Section -->
                    <!-- End Tables Section -->
                </section>
                <?php 
				}
				?>
            </div>

                <!-- Main Section End -->

            </div>
        </section>
    </div>
        <?php include('../footer.php'); ?>
    <!-- simple dialog -->
    <div class="widget modal" id="simpledialog">
        <header><h2>Please Add New Supplier</h2></header>
        <section>
        	<form method="post" action="javascript:insert()">
          	<p>
                Please Fill Requre Fields.
            </p>

            <p>
            <input name="fullname"  id="fullname" type="text" placeholder="Supplier Full Name">
            </p>
            <p>
            <input name="mobile"  id="mobile" type="text" placeholder="Supplier Mobile Number">
            </p>
            
            <p>
                <input type="submit" name="Submit" value="Insert" class="button button-green"/>
                <button type="button" class="button button-red close" onClick="refresh()">Close</button>
            </p>
            </form>
        </section>
    </div>
    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
