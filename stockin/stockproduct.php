<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("../class_file/connection/config.php");	
	$access=$_SESSION['SESS_ID'];
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('../title.php'); ?>

<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        <?php
			  $usr=$_SESSION['SESS_USERNAME'];

				@$sql_check_tab=mysql_num_rows(mysql_query("SELECT * FROM system_admin WHERE username='$usr'"));
				
				
				if($sql_check_tab!=0)
				{
			  ?>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
              <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>All Stock In Product( <?php echo mysql_num_rows(mysql_query("SELECT * FROM `stockin_product`")); ?> )  <span style="position:relative; margin-left:170px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                        </header>
                        <section class="with-table">
                            <table width="97%" class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th width="55">ID</th>
                                      <th width="147" align="center">Barcode Number</th>
                                        <th width="130" align="center">Product Name</th>
                                        <th width="92">Quantity</th>
                                        <th width="120">Total Value</th>
                                      <th width="105">Re-Order</th>
                                      <th width="129">Last Stock-In</th>
                                      <th>Status</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th height="28">ID</th>
                                      <th>Barcode Number</th>
                                        <th>Product Name</th>
                                        <th>Quantity : 
                                        <?php
                                        $sql_qsa=mysql_query("SELECT * FROM stockin_product");
											$qsa=0;
											while($qqsa=mysql_fetch_array($sql_qsa))
											{
												$qsa+=$qqsa['quantity'];	
											}
											echo $qsa;
											
											?>
                                        </th>
                                        <th width="120">Total Value : 
                                        <?php
                                        $sql_qsas=mysql_query("SELECT * FROM stockin_product");
											$qsas=0;
											while($qqsas=mysql_fetch_array($sql_qsas))
											{
												$qsas+=$qqsas['quantity']*$qqsas['unite_price'];	
											}
											echo $qsas;
											
											?>
                                        </th>
                                        <th>Re-Order</th>
                                        <th>Last Stock IN</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                                
                                <tbody>
                                <?php
								$a=1;
								@$sql_bar=mysql_query("SELECT * FROM `product_barcode` order by pbid asc");
								while($rr=mysql_fetch_array($sql_bar))
								{
									$pbid=$rr['pbid'];
									$chk=mysql_num_rows(mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid'"));
									if($chk!=0)
									{
									@$sqlquery=mysql_query("SELECT * FROM `stockin_product` WHERE barcode_id='$pbid' order by spid DESC LIMIT 1");
									$pdata=mysql_fetch_array($sqlquery);
									
									 ?>
										<tr>
											<td align="center"><?php echo $a;  ?></td>
											<td align="center"><?php 
											echo $rr['barcode'];
											?></td>
											<td align="center"><?php echo $rr['p_name']; ?></td>
											<td align="center"><?php
											$sql_qs=mysql_query("SELECT * FROM stockin_product WHERE barcode_id='$pbid'");
											$qs=0;
											while($qqs=mysql_fetch_array($sql_qs))
											{
												$qs+=$qqs['quantity'];	
											}
											echo $qs;
											
											$sql_q=mysql_query("SELECT * FROM stock_out WHERE barcode_id='$pbid'");
											$q=0;
											while($qq=mysql_fetch_array($sql_q))
											{
												$q+=$qq['dquantity'];	
											}
											$q;
											?></td>
                                            <td align="center"><?php
											
											
											$sql_qd=mysql_query("SELECT * FROM stockin_product WHERE barcode_id='$pbid'");
											$qd=0;
											while($qqd=mysql_fetch_array($sql_qd))
											{
												$qd+=$qqd['quantity']*$qqd['unite_price'];	
											}
											echo $qd;
											?></td>
										  <td align="center"><?php $reorder=$rr['reorder'];
										  $resq=$qs-$q;
										  
										  if($resq>$reorder)
										  {
											echo "<span style='background:green; color:white;'>".$reorder."</span>";  
										  }
										  else
										  {
											echo "<span style='background:red; color:white;'>".$reorder."</span>";   
										  }
										
										   ?></td>
										  <td align="center"><?php echo $pdata['spdate']; ?></td>
										  <td width="166" align="center">
                                          
                                           <a href="stockproduct_history.php?id=<?php echo $pbid; ?>&amp;p_name=<?php echo $rr['p_name']; ?>&amp;barcode=<?php echo $rr['barcode']; ?>" class="button button-gray">Record ( <?php echo $qs; ?> )</a>
                                          
                                          </td>
										</tr>
										
										<?php
									
											}
									$a++;
									}
									
									?>
                                </tbody>
                            </table>
                          <div class="container_6 clearfix">
  
                            </div>
                      </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
            </div>

                <!-- Main Section End -->

            </div>
        </section>
        <?php
		}
		
		?>
    </div>
    
		<?php include('../footer.php'); ?>
    <!-- simple dialog -->

    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
