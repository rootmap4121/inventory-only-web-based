<?php	
 session_start();
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_ID']) || (trim($_SESSION['SESS_ID']) == '')) {

			$errmsg_arr[] = 'Login Session Expired Please Login';
			$errflag = true;

	
			//If there are input validations, redirect back to the login form
			if($errflag) {
				$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
				session_write_close();
				header("location: index.php");
				exit();
						}


	}
		$status=$_SESSION['SESS_STATUS'];
		//session_cache_limiter(1000);
	include("../class_file/connection/config.php");	
	$access=$_SESSION['SESS_ID'];
		
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<?php include('../title.php'); ?>

<link rel="stylesheet" media="screen" href="../css/reset.css" />
<link rel="stylesheet" media="screen" href="../css/grid.css" />
<link rel="stylesheet" media="screen" href="../css/style.css" />
<link rel="stylesheet" media="screen" href="../css/messages.css" />
<link rel="stylesheet" media="screen" href="../css/forms.css" />
<link rel="stylesheet" media="screen" href="../css/tables.css" />

<!--[if lt IE 8]>
<link rel="stylesheet" media="screen" href="css/ie.css" />
<![endif]-->

<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<script type="text/javascript" src="js/PIE.js"></script>
<script type="text/javascript" src="js/IE9.js"></script>
<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->

<!-- jquerytools -->
<script type="text/javascript" src="../js/jquery.tools.min.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.ui.min.js"></script>
<script type="text/javascript" src="../js/jquery.tables.js"></script>
<script type="text/javascript" src="../js/jquery.flot.js"></script>

<script type="text/javascript" src="../js/global.js"></script>

<!-- THIS SHOULD COME LAST -->
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie.js"></script>
<![endif]-->

</head>
<body>
    <div id="wrapper">
        <?php include('header_button.php'); ?>
        <?php
			  $usr=$_SESSION['SESS_USERNAME'];

				@$sql_check_tab=mysql_num_rows(mysql_query("SELECT * FROM system_admin WHERE username='$usr'"));
				
				
				if($sql_check_tab!=0)
				{
			  ?>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
			  <div style="padding-top:102px;">
                <section class="main-section grid_8">
                    <!-- Forms Section --><!-- End Forms Section -->
              <div class="clear"></div>

                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2>All Stored Barcode ( <?php echo mysql_num_rows(mysql_query("SELECT * FROM `product_barcode`")); ?> )  <span style="position:relative; margin-left:170px; font:Arial, Helvetica, sans-serif; color:#000000;"><?php
	if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
		foreach($_SESSION['ERRMSG_ARR'] as $msg) {
			echo $msg; 
		}
		unset($_SESSION['ERRMSG_ARR']);
	}
?></span></h2>
                        </header>
                        <section class="with-table">
                            <table class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th width="55">ID</th>
                                      <th width="189" align="center">Product Name</th>
                                        <th width="161" align="center">Barcode Number</th>
                                        <th width="95">Quantity</th>
                                      <th width="139">Re-Order</th>
                                      <th width="156">Price</th>
                                      <th>Status</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th height="28">ID</th>
                                      <th>Product Name</th>
                                        <th>Barcode Number</th>
                                        <th>S-O</th>
                                        <th>Re-Order</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                                
                                <tbody>
                                <?php
								$a=1;
								@$sqlquery=mysql_query("SELECT * FROM `product_barcode` order by pbid asc");
								while($pdata=mysql_fetch_array($sqlquery)){
								
								 ?>
                                    <tr>
                                     	<td align="center"><?php echo $a;  ?></td>
                                        <td align="center"><?php echo $sid=$pdata['p_name']; ?></td>
                                        <td align="center"><?php echo $pid=$pdata['barcode'];
										
										?></td>
                                        <td align="center"><?php
										$pbid=$pdata['pbid'];
										$sqlpbq=mysql_query("SELECT * FROM stockin_product WHERE barcode_id='$pbid'");
										$pbquantity=0;
										while($pbqfet=mysql_fetch_array($sqlpbq))
										{
											$pbquantity+=$pbqfet['quantity'];
										}
										
										$sqlpbdq=mysql_query("SELECT * FROM stock_out WHERE barcode_id='$pbid'");
										$pbdquantity=0;
										while($pbdqfet=mysql_fetch_array($sqlpbdq))
										{
											$pbdquantity+=$pbdqfet['dquantity'];
										}
										echo $pbdquantity;
										echo "-";
										echo $pbquantity;
										?></td>
                                      <td align="center"><?php
										echo $pdata['reorder'];
									  ?></td>
                                      <td align="center"><?php echo $price=$pdata['price'];
									  if($price==0)
									  {
										$color="orange";  
									  }
									  else
									  {
										$color="green";  
									  }
									  
									  ?></td>
                                      <td width="153" align="center">
                                      <a href="#" class="button button-gray view-details">Update Price</a>
                              <div class="overlay-details">
                                                <header class="clearfix">
                                                    <hgroup>
                                                    
                                                        <h2><?php echo $pdata['barcode']; ?></h2>
                                                        <h6>Product Name : <?php echo $pdata['p_name']; ?></h6>
                                                        <h6>Brand Name : <?php echo $pdata['brand']; ?></h6>
                                                        <h6>quantity : <?php echo "Stock In : ".$pbdquantity;
										echo "-";
										echo "Stock Out : ".$pbquantity; ?></h6>
                                                        <h6>Re-Order : <?php echo $pdata['reorder']; ?></h6>
                                                        <h6>Price : <?php echo $pdata['price']; ?></h6>    
                                                        <h6>Added : <?php echo $pdata['pbdate']; ?></h6>
                            
                                                    </hgroup>
                                                </header>
                                                <section>
                                                <form action="../class_file/update_barcode_price.php" method="post">
                                                    <table class="simple full">
                                                    <thead>
                                    <tr>
                                        <th width="55">Barcode</th>
                                        <th align="center">Brand Name</th>
                                      <th align="center">Price</th>
                                        
                                        
                                  </tr>
                                </thead> <tbody>
                               
                              
                                <tr>
                                    <td>
                                    <input type="text" class="medium" name="barcode" value="<?php echo $pdata['barcode']; ?>" placeholder="Barcode New"><input name="pbid" id="pbid" type="hidden" value="<?php echo $pbid; ?>">
                                    </td>
                                    <td>
                                    <input type="text" class="medium" name="brand" id="brand" value="<?php echo $pdata['brand']; ?>" placeholder="New Brand"></td><td>
                                    <input type="text" class="medium" name="price" id="price" value="<?php echo $pdata['price']; ?>" placeholder="New Price"></td>
                                 
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" valign="middle">
                                    <input type="submit" class="button button-green" value="Update">
                                    </td>
                                   
                                 
                                </tr>
                                                  
                                                            
                                                        </tbody>
                                                    </table>
                                                    </form>
                                                </section>
                                            </div></td>
                                    </tr>
                                    
                                    <?php  
									$a++;
									}  
									?>
                                </tbody>
                            </table>
                          <div class="container_6 clearfix">
  
                            </div>
                      </section>
                    </div>
                    <!-- End Tables Section -->
                </section>
            </div>

                <!-- Main Section End -->

            </div>
        </section>
        <?php
		}
		
		?>
    </div>
    
		<?php include('../footer.php'); ?>
    <!-- simple dialog -->

    <!-- end simple dialog -->

<script>
$(function () {
    /**
     * Modal Dialog Boxes Setup
     */

    var triggers = $(".modalInput").overlay({

        // some mask tweaks suitable for modal dialogs
        mask: {
            color: '#000',
            loadSpeed: 200,
            opacity: 0.5
        },

        closeOnClick: false
    });

    /* Simple Modal Box */
    var buttons1 = $("#simpledialog button").click(function(e) {
	
        // get user input
        var yes = buttons1.index(this) === 0;

        if (yes) {
            // do the processing here
        }
    });

});
</script>

</body>
</html>
